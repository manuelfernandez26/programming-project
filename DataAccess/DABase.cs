﻿using Entities;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataAccess
{
    public class DABase : IDisposable
    {
        public ENBase DatesEntitie;

        protected Database DBConexion { get; set; }

        public string ResultMessage { get; set; }
        public Enumerated.ResultTypes ResultType { get; set; }

        protected void CreateConexionDB(string DBName)
        {
            if (DBConexion == null)
            {
                DatabaseProviderFactory factory = new DatabaseProviderFactory();
                DBConexion = factory.Create(DBName);
            }
        }
        public void SaveError(Exception exception)
        {
            if (Constant.Logs.RutLog != string.Empty)
            {
                using (StreamWriter file = new StreamWriter(Constant.Logs.RutLog, true))
                {
                    file.WriteLine(exception.Message + " - " + exception.StackTrace);
                }
            }
        }
        public void Dispose()
        {
            if (DBConexion != null)
            {
                DBConexion = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
