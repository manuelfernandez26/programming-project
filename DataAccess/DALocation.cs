﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataAccess
{
    public class DALocation : DABase
    {
        public int NewLocationId { get; set; }
        public List<ENLocation> ListAllLocation()
        {
            List<ENLocation> locationList = new List<ENLocation>();
            try
            {
                CreateConexionDB(Constant.DBNames.BDProgrammingProyect);
                using (DbCommand command = DBConexion.GetStoredProcCommand(Query.Location.ListAllLocation))
                {
                    using (IDataReader reader = DBConexion.ExecuteReader(command))
                    {
                        int locationColumn = reader.GetOrdinal("LocationId");
                        int nameColumn = reader.GetOrdinal("Name");
                        int descriptionColumn = reader.GetOrdinal("Description");
                        int LongitudeColumn = reader.GetOrdinal("Longitude");
                        int LatitudeColumn = reader.GetOrdinal("Latitude");
                        while (reader.Read())
                        {
                            ENLocation location = new ENLocation();
                            location.LocationId = Convert.ToInt32(reader[locationColumn].ToString());
                            location.Name = reader[nameColumn].ToString();
                            location.Description = reader[descriptionColumn].ToString();
                            location.Longitude = Convert.ToDecimal(reader[LongitudeColumn].ToString());
                            location.Latitude = Convert.ToDecimal(reader[LatitudeColumn].ToString());
                            locationList.Add(location);
                        }
                    }
                }
                ResultType = Enumerated.ResultTypes.Success;
                return locationList;
            }
            catch (Exception exception)
            { 
                ResultType = Enumerated.ResultTypes.Error;
                ResultMessage = Message.Location.ErrorListLocation;
                SaveError(exception);
                return new List<ENLocation>();
            }
        }

        public void CreateLocation(ENLocation NewLocation)
        {
            try
            {
                CreateConexionDB(Constant.DBNames.BDProgrammingProyect);
                using (DbCommand command = DBConexion.GetStoredProcCommand(Query.Location.CreateLocation))
                {
                    DBConexion.AddInParameter(command, "@Name", DbType.String, NewLocation.Name);
                    DBConexion.AddInParameter(command, "@Description", DbType.String, NewLocation.Description);
                    DBConexion.AddInParameter(command, "@Longitude", DbType.String, NewLocation.Longitude.ToString());
                    DBConexion.AddInParameter(command, "@Latitude", DbType.String, NewLocation.Latitude.ToString());
                    DBConexion.AddOutParameter(command, "@NewLocationId", DbType.Int32, 20);
                    DBConexion.ExecuteNonQuery(command);
                    NewLocationId = Convert.ToInt32(DBConexion.GetParameterValue(command, "NewLocationId").ToString());
                    ResultType = Enumerated.ResultTypes.Success;
                    ResultMessage = Message.Location.SuccessCreateLocation;
                }
            }
            catch (Exception exception)
            {
                ResultType = Enumerated.ResultTypes.Error;
                ResultMessage = Message.Location.ErrorCreateLocation;
                SaveError(exception);
            }
        }

        public void UpdateLocation(int LocationID, Decimal Latitude, Decimal Longitude)
        {
            try
            {
                CreateConexionDB(Constant.DBNames.BDProgrammingProyect);
                using (DbCommand command = DBConexion.GetStoredProcCommand(Query.Location.UpdateLocation))
                {
                    DBConexion.AddInParameter(command, "@LocationId", DbType.Int32, LocationID);
                    DBConexion.AddInParameter(command, "@Longitude", DbType.String, Longitude.ToString());
                    DBConexion.AddInParameter(command, "@Latitude", DbType.String, Latitude.ToString());
                    DBConexion.ExecuteNonQuery(command);
                    ResultType = Enumerated.ResultTypes.Success;
                    ResultMessage = Message.Location.SuccessUpdateLocation;
                }
            }
            catch (Exception exception)
            {
                ResultType = Enumerated.ResultTypes.Error;
                ResultMessage = Message.Location.ErrorUpdateLocation;
                SaveError(exception);
            }
        }
    }
}
