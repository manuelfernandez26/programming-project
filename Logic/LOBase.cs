﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Logic
{
    public class LOBase
    {
        public string ResultMessage { get; set; }
        public Enumerated.ResultTypes ResultType { get; set; }
    }
}
