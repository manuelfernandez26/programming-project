﻿using DataAccess;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class LOLocation : LOBase
    {
        public int NewLocationId { get; set; }
        public List<ENLocation> ListAllLocation()
        {
            using (DALocation daLocation = new DALocation())
            {
                List<ENLocation> locationList = daLocation.ListAllLocation();
                ResultType = daLocation.ResultType;
                ResultMessage = daLocation.ResultMessage;
                return locationList;
            }
        }
        public void CreateLocation(ENLocation NewLocation)
        {
            using (DALocation daLocation = new DALocation())
            {
                daLocation.CreateLocation(NewLocation);
                NewLocationId = daLocation.NewLocationId;
                ResultType = daLocation.ResultType;
                ResultMessage = daLocation.ResultMessage;
            }
        }
        public void UpdateLocation(int LocationID, Decimal Latitude, Decimal Longitude)
        {
            using (DALocation daLocation = new DALocation())
            {
                daLocation.UpdateLocation(LocationID, Latitude, Longitude);
                ResultType = daLocation.ResultType;
                ResultMessage = daLocation.ResultMessage;
            }
        }
    }
}
