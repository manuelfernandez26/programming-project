﻿using DataAccess;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programming_Project.Tests.Data_Access
{
    [TestFixture]
    public class DABaseTest
    {
        [Test]
        public void SaveError()
        {
            Exception exception = new Exception("Error Test");
            DABase adBase = new DABase();
            adBase.SaveError(exception);
        }
    }
}
