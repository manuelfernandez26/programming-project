﻿using DataAccess;
using Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Programming_Project.Tests.Data_Access
{
    [TestFixture]
    public class DALocationTest
    {
        [Test]
        public void ListAllLocationTest()
        {
            List<ENLocation> locationList = new List<ENLocation>();
            using (DALocation daLocation = new DALocation())
            {
                locationList = daLocation.ListAllLocation();
            }
            Assert.IsTrue(locationList.Count > 0);
        }

        [Test]
        public void CreateLocationTest()
        {
            Enumerated.ResultTypes result;
            ENLocation newLocation = new ENLocation();
            newLocation.Name = "Mark 4";
            newLocation.Description = "Place to shopp cars.";
            newLocation.Longitude = -77.039863M;
            newLocation.Latitude = -12.100666M;
            int newLocationId = 0;
            using (DALocation daLocation = new DALocation())
            {
                daLocation.CreateLocation(newLocation);
                result = daLocation.ResultType;
                newLocationId = daLocation.NewLocationId;
            }
            Assert.IsTrue(result == Enumerated.ResultTypes.Success);
        }

        [Test]
        public void UpdateLocationTest()
        {
            Enumerated.ResultTypes result;
            int locationId = 21;
            decimal longitude = -77.039000M;
            decimal latitude = -12.1006000M;
            using (DALocation daLocation = new DALocation())
            {
                daLocation.UpdateLocation(locationId, latitude, longitude);
                result = daLocation.ResultType;
            }
            Assert.IsTrue(result == Enumerated.ResultTypes.Success);
        }
    }
}