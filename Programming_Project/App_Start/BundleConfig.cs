﻿using System.Web;
using System.Web.Optimization;

namespace Programming_Project
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-1.10.2.js")
                .Include("~/Scripts/jquery-1.10.2.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/Common/common").Include(
                "~/Scripts/Common/jsCommon.js",
                "~/Scripts/Common/jsKendoWindow.js"));

            bundles.Add(new ScriptBundle("~/bundles/kendoJS")
                        .Include("~/Scripts/kendo/2014.1.415/jquery.min.js")
                        .Include("~/Scripts/kendo/2014.1.415/kendo.all.min.js")
                        .Include("~/Scripts/kendo/2014.1.415/kendo.aspnetmvc.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizers")
                       .Include("~/Scripts/kendo.modernizr.custom.js"));


            bundles.Add(new StyleBundle("~/Content/cssKendo").Include(
                     "~/Content/kendo/2014.1.415/kendo.common.min.css",
                     "~/Content/kendo/2014.1.415/kendo.dataviz.min.css",
                     "~/Content/kendo/2014.1.415/kendo.bootstrap.min.css",
                     "~/Content/kendo/2014.1.415/kendo.dataviz.bootstrap.min.css"
                     ));
        }
    }
}
