﻿using Entities;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Programming_Project.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public JsonResult ListAllLocation()
        {
            LOLocation loLocation = new LOLocation();
            List<ENLocation> locationList = loLocation.ListAllLocation();
            return this.Json(new { Response = (int)loLocation.ResultType, Message = loLocation.ResultMessage, LocationList = locationList });
        }

        public JsonResult CreateLocation(ENLocation NewLocation)
        {
            LOLocation loLocation = new LOLocation();
            loLocation.CreateLocation(NewLocation);
            return this.Json(new { Response = (int)loLocation.ResultType, Message = loLocation.ResultMessage, Id = loLocation.NewLocationId });
        }

        public JsonResult UpdateLocation(int LocationID, Decimal Latitude, Decimal Longitude)
        {
            LOLocation loLocation = new LOLocation();
            loLocation.UpdateLocation(LocationID, Latitude, Longitude);
            return this.Json(new { Response = (int)loLocation.ResultType, Message = loLocation.ResultMessage });
        }
    }
}