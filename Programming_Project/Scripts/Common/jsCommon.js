﻿function setImageOnRequest() {
    $("#loading-panel").css("display", "block");
    $("#loading-panel").html('<div class="panel-load"><img src="' + $("#urlloading").val() + '" width="48" height="48" alt="por favor, espere..." /> </div>');
}

function removeImageOnRequest() {
    $("#loading-panel").css("display", "none");
    $("#loading-panel").empty();
}

function showResultInDiv(DivName, Message, Option) {

    $('#' + DivName).html('');

    switch (Option) {
        case 1: {
            Message = $('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">×</button>' +
                            '<strong style="font-size:12pt;"><i class="icon-ok-sign icon-white" style="width:20px;height:20px;"></i>Success. </strong> ¡ ' + Message + ' ! </div>');
            break;
        }
        case 2: {
            if (Message == "")
                Message = createGenericMessageError();
            Message = $('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">×</button>' +
                           '<strong style="font-size:12pt;"><i class="icon-warning-sign icon-white" style="width:20px;height:20px;"></i>Error. </strong> ' + Message + ' </div>');
            break;
        }
        case 3: {
            Message = $('<div class="alert alert-info"> <button type="button" class="close" data-dismiss="alert">×</button>' +
                            '<strong style="font-size:12pt;"><i class="icon-ok-sign icon-white" style="width:20px;height:20px;"></i>Info. </strong>  ' + Message + '</div>');
            break;
        }
    }
    $('#' + DivName).append(Message);
}

function createGenericMessageError() {
    return "An error has ocurried. Please try again later."
}