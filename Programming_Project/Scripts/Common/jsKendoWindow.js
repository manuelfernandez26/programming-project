﻿function KendoWindow_OpenPopup(PopupName) {
    $("#" + PopupName).data("kendoWindow").open().center();
}

function KendoWindow_ClosePopup(PopupName) {
    $("#" + PopupName).data("kendoWindow").close();
}