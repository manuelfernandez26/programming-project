﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Programming_Project.Startup))]
namespace Programming_Project
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
