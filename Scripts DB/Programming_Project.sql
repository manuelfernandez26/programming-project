USE [Programming_Project]
GO
/****** Object:  StoredProcedure [dbo].[CreateLocation]    Script Date: 05/05/2014 05:13:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[CreateLocation]
@Name VARCHAR(50),
@Description NVARCHAR(300),
@Longitude NVARCHAR(100),
@Latitude NVARCHAR(100),
@NewLocationId INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO Location
	(Name,Description,Longitude,Latitude,State)
	VALUES
	(@Name,@Description,@Longitude,@Latitude,1)
	SET @NewLocationId = @@IDENTITY
END

GO
/****** Object:  StoredProcedure [dbo].[ListAllLocation]    Script Date: 05/05/2014 05:13:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Manuel Fernandez
-- Create date: 03-05-2014
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ListAllLocation] 
AS
BEGIN
	SET NOCOUNT ON;
	SELECT LocationId,Name,Description,Longitude,Latitude FROM Location
	WHERE State = 1
END

GO
/****** Object:  StoredProcedure [dbo].[UpdateLocation]    Script Date: 05/05/2014 05:13:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[UpdateLocation]
@LocationId INT,
@Longitude NVARCHAR(100),
@Latitude NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Location
	SET Longitude = @Longitude, Latitude = @Latitude
	WHERE LocationId = @LocationId
END

GO
/****** Object:  Table [dbo].[Location]    Script Date: 05/05/2014 05:13:26 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [nvarchar](300) NULL,
	[Longitude] [nvarchar](100) NOT NULL,
	[Latitude] [nvarchar](100) NOT NULL,
	[State] [bit] NOT NULL,
 CONSTRAINT [PK_Location] PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
