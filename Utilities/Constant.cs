﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public abstract class Constant
    {
        public class DBNames
        {
            public const string BDProgrammingProyect = "BDProgrammingProyect";
        }
        public class DefaultCoordinate
        {
            public static string Latitude { get { return ConfigurationManager.AppSettings["DefaultLatitude"] == null ? "0" : ConfigurationManager.AppSettings["DefaultLatitude"].ToString(); } }
            public static string Longitude { get { return ConfigurationManager.AppSettings["DefaultLongitude"] == null ? "0" : ConfigurationManager.AppSettings["DefaultLongitude"].ToString(); } }
        }

        public class Logs
        {
            public static string RutLog { get { return ConfigurationManager.AppSettings["RutLog"] == null ? "" : ConfigurationManager.AppSettings["RutLog"].ToString(); } }
        }
    }
}
