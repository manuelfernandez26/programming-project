﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class Message
    {
        public class Location
        {
            public const string ErrorListLocation = "An error occurred when listing the locations.";
            public const string SuccessCreateLocation = "New location was created correctly.";
            public const string ErrorCreateLocation = "An error occurred while creating the location.";
            public const string SuccessUpdateLocation = "Location was updated correctly.";
            public const string ErrorUpdateLocation = "An error occurred while updating the location.";
        }
    }
}
