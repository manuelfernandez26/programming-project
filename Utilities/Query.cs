﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class Query
    {
        public class Location
        {
            public const string ListAllLocation = "ListAllLocation";
            public const string CreateLocation = "CreateLocation";
            public const string UpdateLocation = "UpdateLocation";
        }
    }
}
